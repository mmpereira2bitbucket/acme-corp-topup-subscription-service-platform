package com.acme.app.topup.telco.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.time.LocalDate;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.validation.ExtendedValidationException;

@TestPropertySource(properties = { "spring.aop.proxy-target-class=true" })
@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan(basePackages = { "com.acme.app.topup" })
@EntityScan(basePackages = { "com.acme.app.topup" })
@EnableJpaRepositories(basePackages = { "com.acme.app.topup" })
public class AbstractServiceTester extends AbstractTester {

	public AbstractServiceTester() {
	}

	protected void checkValidation(ExtendedValidationException e, String objectName, String field, Object value) {
		assertNotNull(e.getErrors());
		assertNotNull(e.getErrors().getFieldError());
		assertEquals(field, e.getErrors().getFieldError().getField());
		assertEquals(objectName, e.getErrors().getFieldError().getObjectName());
		assertEquals(value, e.getErrors().getFieldError().getRejectedValue());
	}

	protected void checkOfferingCost(OfferingCost offeringCost, LocalDate validFrom, LocalDate validUntil,
			Double cost) {
		checkOfferingCost(offeringCost, null == validFrom ? null : Date.valueOf(validFrom),
				null == validUntil ? null : Date.valueOf(validUntil), cost);
	}

	protected void checkOfferingCost(OfferingCost offeringCost, Date validFrom, Date validUntil, Double cost) {
		assertNotNull(offeringCost);
		assertNotNull(offeringCost.getOffering());
		assertEquals(validFrom, offeringCost.getValidFrom());
		assertEquals(validUntil, offeringCost.getValidUntil());
		assertEquals(cost, (Double) offeringCost.getCost());
		assertTrue("OfferingCost.id > 0", offeringCost.getId() > 0);
	}

	protected void checkOffering(Offering offering, Telco telco, String offeringId, Offering.Status status) {
		checkOffering(offering, null == telco ? null : telco.getId(), offeringId, status);
	}

	protected void checkOffering(Offering offering, String telco, String offeringId, Offering.Status status) {
		assertNotNull(offering);
		assertNotNull(offering.getOwner());
		assertEquals(telco, offering.getOwner().getId());
		assertNotNull(offering.getExternalId());
		assertEquals(offeringId, offering.getExternalId());
		assertNotNull(offering.getStatus());
		assertEquals(status, offering.getStatus());
		assertTrue("Offering.id > 0", offering.getId() > 0);
	}
}
