package com.acme.app.topup.event;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface EventSource {
	/**
	 * EventEnricher that must be used when intercepting events created by the
	 * method interception. Subevents are also notified to this enricher that
	 * can enrich its events or not with new information.
	 */
	Class<? extends EventEnricher>[] enricher() default {};
}
