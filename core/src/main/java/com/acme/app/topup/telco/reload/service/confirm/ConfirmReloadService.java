package com.acme.app.topup.telco.reload.service.confirm;

import java.io.IOException;
import java.util.UUID;

import com.acme.app.topup.telco.reload.service.TelcoResponse;
import com.acme.app.topup.validation.ExtendedValidationException;

public interface ConfirmReloadService {

	class Output extends TelcoResponse {
		public Output(String id) {
			super(id);
		}

		String reloadRefForProbing = null;
	}
	
	Output execute(UUID requestId, String msisdn, String offeringExternalId, double cost) throws ExtendedValidationException, IOException;
}
