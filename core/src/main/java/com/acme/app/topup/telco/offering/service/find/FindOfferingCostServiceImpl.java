package com.acme.app.topup.telco.offering.service.find;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.offering.model.dao.OfferingCostDAO;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;

@Named
public class FindOfferingCostServiceImpl implements FindOfferingCostService {

	private OfferingCostDAO offeringCostDAO;
	
	@Inject
	public FindOfferingCostServiceImpl(OfferingCostDAO offeringCostDAO) {
		super();
		Assert.notNull(offeringCostDAO, "OfferingCostDAO is mandatory");
		this.offeringCostDAO = offeringCostDAO;
	}

	@Override
	public OfferingCost execute(Offering offering, LocalDate validUntil) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.ID, offering);
		Assert.mandatory(Offering.Field.ID, offering.getId());
		
		Date until = null == validUntil? null: Date.valueOf(validUntil);
		
		List<OfferingCost> list = offeringCostDAO.findActiveCostsForDateUsingId(offering.getId(), until);
		OfferingCost result = null;
		if (null != list && list.size() > 0) {
			result = list.get(0);
////			result.offeringId = input.offeringId;
////			result.cost = offeringCost.getCost();
////			result.status = offeringCost.getOffering().getStatus();
////			result.validFrom = offeringCost.getValidFrom().toLocalDate();
////			until = offeringCost.getValidUntil();
//			if (null != until) {
//				result.validUntil = until.toLocalDate();
//			}
		}
		
		return result;
	}

	@Override
	public List<OfferingCost> execute(Telco telco, LocalDate validUntil) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telco);
		Assert.mandatory(Offering.Field.OWNER, telco.getId());
		Date until = null == validUntil? null: Date.valueOf(validUntil);
		
		return offeringCostDAO.findCostsUsingTelcoId(telco.getId(), until);
	}

}
