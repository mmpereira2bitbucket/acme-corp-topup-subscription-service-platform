package com.acme.app.topup.event;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.ExtendedValidationException.ValidationType;

public class EventManager {
	class EventSourceInfo {
		Method annotatedMethod = null;
		String annotatedPath;
		EventEnricher[] eventEnrichers = null;
		String realPath;
	}

	private static final Log LOGGER = LogFactory.getLog(EventManager.class);

	static private EventManager singleton = new EventManager();

	static public EventManager getInstance() {
		return singleton;
	}

	private Set<EventListener> eventListenerSet = new HashSet<>();
	private Map<Method, EventSourceInfo> eventSourceMethods = new HashMap<>();
	private Executor executor = Executors.newCachedThreadPool();
	private ThreadLocal<Stack<Event>> threadEventStack = new ThreadLocal<>();

	private EventManager() {
	}

	/**
	 * Create a new event
	 * 
	 * @param source
	 * @return
	 */
	Event createEvent(Method source) {
		Event result = null;
		EventSourceInfo eventSourceInfo = getEventSourceInfo(source);
		if (null != eventSourceInfo) {
			result = new Event(source);
			getEventStack().push(result);
			for (EventEnricher enricher : eventSourceInfo.eventEnrichers) {
				try {
					if (null != enricher)
						enricher.onCreation(result);
				} catch (Throwable ex) {
					LOGGER.error(
							String.format("Failed to invoke EventEnricher[%s] for onCreation(%s)", enricher, result),
							ex);
				}
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Event created by %s as %s", eventSourceInfo.realPath,
						eventSourceInfo.annotatedPath));
			}
		}

		return result;
	}

	/**
	 * If event source info does not exist just return null to avoi event
	 * creation or manipulation
	 * 
	 * @param source
	 *            method source of the event
	 * @return EventSourceInfo associated with given method
	 */
	protected EventSourceInfo getEventSourceInfo(Method source) {
		EventSourceInfo result = null;
		if (null != source) {
			try {
				result = Assert.notNull(this.eventSourceMethods.get(source),
						() -> new ExtendedValidationException(EventManager.class, "eventSourceMethods", source.toString(),
								ValidationType.NOT_FOUND));
			} catch (ExtendedValidationException e) {
				LOGGER.error("Failed to create event for method " + source, e);
			}
		}

		return result;
	}

	/**
	 * If the event stack was not initialized, then create one and return it.
	 * 
	 * @return the event stack associated with the current thread.
	 */
	protected Stack<Event> getEventStack() {
		Stack<Event> eventStack = this.threadEventStack.get();
		if (null == eventStack) {
			eventStack = new Stack<>();
			this.threadEventStack.set(eventStack);
		}

		return eventStack;
	}

	public boolean isEventSourceRegistered(Method realMethod) {
		return this.eventSourceMethods.containsKey(realMethod);
	}

	/**
	 * Notify manager a method terminated with error
	 * 
	 * @param event
	 * @param failure
	 */
	public void notifyFailure(Event event, Throwable failure) {
		if (null != event) {
			event.endTimestamp = Instant.now();
			event.exceptionThrown = failure;
			EventSourceInfo eventSourceInfo = getEventSourceInfo(event.getSource());
			if (null != eventSourceInfo) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(String.format("Failure %s notified in %s by %s",
							null == failure ? "null" : failure.getClass().getSimpleName(), event,
							eventSourceInfo.realPath));
				}

				for (EventEnricher enricher : eventSourceInfo.eventEnrichers) {
					try {
						if (null != enricher)
							enricher.onException(event, failure);
					} catch (Throwable ex) {
						LOGGER.error(String.format("Failed to invoke EventEnricher[%s] for onException(%s, %s)",
								enricher, event, failure), ex);
					}
				}
			}
			if (removeEventFromStack(event)) {
				/*
				 * Notify listener only about the first event created, subevents
				 * are not notified, but can be manipulated by the EventEnricher
				 * instances that can add it as a subevent to the main event.
				 */
				publish(event);
			}
		}
	}

	/**
	 * Notify manager a method terminated with success
	 * 
	 * @param event
	 * @param methodResult
	 */
	public void notifySuccess(Event event, Object methodResult) {
		if (null != event) {
			event.endTimestamp = Instant.now();
			EventSourceInfo eventSourceInfo = getEventSourceInfo(event.getSource());
			if (null != eventSourceInfo) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(String.format("Success return [%s] notified in %s by %s", methodResult, event,
							eventSourceInfo.realPath));
				}

				for (EventEnricher enricher : eventSourceInfo.eventEnrichers) {
					try {
						if (null != enricher)
							enricher.onSuccess(event, methodResult);
					} catch (Throwable ex) {
						LOGGER.error(String.format("Failed to invoke EventEnricher[%s] for onSuccess(%s, %s)", enricher,
								event, methodResult), ex);
					}
				}
			}
			if (removeEventFromStack(event)) {
				/*
				 * Notify listener only about the first event created, subevents
				 * are not notified, but can be manipulated by the EventEnricher
				 * instances that can add it as a subevent to the main event.
				 */
				publish(event);
			}
		}
	}

	/**
	 * Publish the event to all listeners in a separated thread.
	 * 
	 * @param event
	 */
	protected void publish(Event event) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("publishing %s", event));
		}

		this.executor.execute(() -> {
			for (EventListener listener : this.eventListenerSet) {
				try {
					listener.onEvent(event);
				} catch (Throwable ex) {
					LOGGER.error(String.format("Listenet[%s] thrown exception for %s", listener, event), ex);
				}
			}
		});
	}

	public void registerEventListener(EventListener eventListener) {
		this.eventListenerSet.add(eventListener);
	}

	public void registerEventSource(Method realMethod, Method annotatedMethod, Class<?> targetClass) {
		if (realMethod.getDeclaringClass().isInterface() == false && Proxy.isProxyClass(realMethod.getDeclaringClass()) == false) {
			EventSourceInfo eventSourceInfo = this.eventSourceMethods.get(realMethod);
			EventSource annotation = annotatedMethod.getAnnotation(EventSource.class);
			if (null == eventSourceInfo && null != annotation) {
				
				eventSourceInfo = new EventSourceInfo();
				eventSourceInfo.annotatedMethod = annotatedMethod;
				eventSourceInfo.realPath = String.format("%s.%s", realMethod.getDeclaringClass().getName(),
						realMethod.getName());
				eventSourceInfo.annotatedPath = String.format("%s.%s", annotatedMethod.getDeclaringClass().getName(),
						annotatedMethod.getName());
				Class<? extends EventEnricher> enricherClasses[] = annotation.enricher();
				eventSourceInfo.eventEnrichers = new EventEnricher[enricherClasses.length];
				int i = 0;
				for (Class<? extends EventEnricher> enricherClass : enricherClasses) {
					try {
						eventSourceInfo.eventEnrichers[i] = enricherClass.newInstance();
					} catch (Throwable e) {
						eventSourceInfo.eventEnrichers[i] = null;
						LOGGER.error(String.format("Failed to create EventEnricher[%s] for method %s", enricherClass,
								realMethod), e);
					}
				}

				this.eventSourceMethods.put(realMethod, eventSourceInfo);
				LOGGER.info(
						String.format("Registering %s[%d] as %s", eventSourceInfo.realPath, realMethod.hashCode(), eventSourceInfo.annotatedPath));
			}
		}
	}

	/**
	 * Remove the given event from the stack and if it is the last one, then
	 * return true.
	 * 
	 * @param event
	 * @return true if the given event is the last one
	 */
	protected boolean removeEventFromStack(Event event) {
		boolean result = false;
		Stack<Event> eventStack = getEventStack();
		if (eventStack.peek() == event) {
			eventStack.pop();
			result = eventStack.isEmpty();
		} else {
			eventStack.remove(event);
		}

		return result;
	}
}
