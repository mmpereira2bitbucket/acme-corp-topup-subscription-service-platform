package com.acme.app.topup.telco.offering.service.create;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.service.AbstractServiceTester;
import com.acme.app.topup.validation.ExtendedValidationException;

@SpringBootTest(classes = { TestCreateOfferingCostServiceImpl.class })
public class TestCreateOfferingCostServiceImpl extends AbstractServiceTester {
	private static final Telco TELCO = new Telco("TELCO");
	private static final String TURBO_OFFER = "TURBO";
	private static final LocalDate TODAY = LocalDate.now();
	private static final LocalDate TOMORROW = TODAY.plus(1, ChronoUnit.DAYS);
	private static final LocalDate YESTERDAY = TODAY.minus(1, ChronoUnit.DAYS);
	private static final LocalDate DAY_BEFORE_YESTERDAY = YESTERDAY.minus(1, ChronoUnit.DAYS);

	private CreateOfferingCostServiceImpl createOfferingCostServiceImpl;
	
	@Before
	public void setup() {
		this.createOfferingCostServiceImpl = new CreateOfferingCostServiceImpl(offeringDAO, offeringCostDAO);
	}
	
	@Test
	public void testMissingDateFrom() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			this.createOfferingCostServiceImpl.execute(offering, null, null, 10.00);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, OfferingCost.class.getName(), "validFrom", null);
		}
	}

	@Test
	public void testZeroCost() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			this.createOfferingCostServiceImpl.execute(offering, LocalDate.of(2017, 01, 01), null, 0.0);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, OfferingCost.class.getName(), "cost", 0.0);
		}
	}

	@Test
	public void testNegativeCost() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			this.createOfferingCostServiceImpl.execute(offering, LocalDate.of(2017, 01, 01), null, -1.0);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, OfferingCost.class.getName(), "cost", -1.0);
		}
	}

	@Test
	public void testValidUntilBeforeValidFrom() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			this.createOfferingCostServiceImpl.execute(offering, TODAY, YESTERDAY, 10.00);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, OfferingCost.class.getName(), "validUntil", YESTERDAY);
		}
	}

	@Test
	public void testValidUntilInThePast() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			this.createOfferingCostServiceImpl.execute(offering, DAY_BEFORE_YESTERDAY, YESTERDAY, 10.00);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, OfferingCost.class.getName(), "validUntil", YESTERDAY);
		}
	}

	@Test
	public void testSuccessCreationWithValidFromInThePastAndNoValidity() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			OfferingCost offeringCost = this.createOfferingCostServiceImpl.execute(offering, YESTERDAY, null, 10.00);

			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, Date.valueOf(YESTERDAY), null, 10.00);
			checkOffering(offeringCost.getOffering(), TELCO.getId(), TURBO_OFFER, Offering.Status.ACTIVE);

			assertNotNull(offeringDAO.findByExternalId(TELCO.getId(), TURBO_OFFER));
			List<OfferingCost> list = offeringCostDAO.searchCostFor(TURBO_OFFER, Date.valueOf(TODAY));
			assertNotNull(list);
			assertEquals(1, list.size());
			offeringCost = list.get(0);
			assertEquals((Double) 10.00, (Double) offeringCost.getCost());
			assertEquals(Date.valueOf(YESTERDAY), offeringCost.getValidFrom());
			assertTrue(null == offeringCost.getValidUntil());
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testSuccessCreationWithValidFromAndValidUntilInTheSameDay() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			OfferingCost offeringCost = this.createOfferingCostServiceImpl.execute(offering, TODAY, TODAY, 10.00);

			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, Date.valueOf(TODAY), Date.valueOf(TODAY), 10.00);
			checkOffering(offeringCost.getOffering(), TELCO.getId(), TURBO_OFFER, Offering.Status.ACTIVE);
			assertNotNull(offeringDAO.findByExternalId(TELCO.getId(), TURBO_OFFER));
			List<OfferingCost> list = offeringCostDAO.searchCostFor(TURBO_OFFER, Date.valueOf(TODAY));
			assertNotNull(list);
			assertEquals(1, list.size());
			offeringCost = list.get(0);
			assertEquals((Double) 10.00, (Double) offeringCost.getCost());
			assertEquals(Date.valueOf(TODAY), offeringCost.getValidFrom());
			assertEquals(Date.valueOf(TODAY), offeringCost.getValidUntil());

		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testSuccessCreationWithValidFromInThePastAndWithValidityInTheFuture() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			OfferingCost offeringCost = this.createOfferingCostServiceImpl.execute(offering, YESTERDAY, TOMORROW, 10.00);

			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, Date.valueOf(YESTERDAY), Date.valueOf(TOMORROW), 10.00);
			checkOffering(offeringCost.getOffering(), TELCO.getId(), TURBO_OFFER, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testDuplicateOfferingCost() {
		try {
			Offering offering = this.createOffering(TELCO, TURBO_OFFER);
			OfferingCost offeringCost = this.createOfferingCostServiceImpl.execute(offering, YESTERDAY, TOMORROW, 10.00);

			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, Date.valueOf(YESTERDAY), Date.valueOf(TOMORROW), 10.00);
			checkOffering(offeringCost.getOffering(), TELCO.getId(), TURBO_OFFER, Offering.Status.ACTIVE);

			offeringCost = this.createOfferingCostServiceImpl.execute(offering, YESTERDAY, TOMORROW, 10.00);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			String periodExpectedInTheRejectedValue = String.format("[%s;%s]", YESTERDAY, TOMORROW);
			checkValidation(e, OfferingCost.class.getName(), "cost", periodExpectedInTheRejectedValue);
		} catch (Throwable e) {
			e.printStackTrace();
			fail("Not Expected ValitationException");
		}
	}
}

