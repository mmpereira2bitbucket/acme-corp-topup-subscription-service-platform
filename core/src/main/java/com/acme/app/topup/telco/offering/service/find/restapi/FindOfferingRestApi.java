package com.acme.app.topup.telco.offering.service.find.restapi;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.acme.app.topup.service.restapi.RestApi;
import com.acme.app.topup.validation.ExtendedValidationException;

public interface FindOfferingRestApi extends RestApi {

	class Output {
		public String offeringId = null;
		public String validFrom = null; // YYYYMMDD
		public String validUntil = null; // YYYYMMDD
		public Double cost = null;
		public String status = null;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { 
			"offering/{id}", 
		}, method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	Output findOneOffering(
			@PathVariable("telcoId") String telcoId, 
			@PathVariable("id") String offeringExternalId) throws ExtendedValidationException;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { 
			"offering/{id}?date={atDate}", 
		}, method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	Output findOneOffering(
			@PathVariable("telcoId") String telcoId, 
			@PathVariable("id") String offeringExternalId,
			@PathVariable("atDate") String atDate) throws ExtendedValidationException;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { 
			"offerings?date={atDate}", 
		}, method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	List<Output> findOfferings(
			@PathVariable("telcoId") String telcoId, 
			@PathVariable("atDate") String atDate) throws ExtendedValidationException;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { 
			"offerings", 
		}, method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	List<Output> findOfferings(
			@PathVariable("telcoId") String telcoId) throws ExtendedValidationException;
}
