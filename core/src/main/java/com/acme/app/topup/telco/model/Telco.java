package com.acme.app.topup.telco.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.acme.app.topup.util.FieldFinder;

/**
 * Telco is an entity that represents the Telecommunication Company
 */
@Entity
public class Telco {
	/**
	 * Identification of the Telco as TIM, CLARO, VIVO, etc
	 * 
	 * @return identification
	 */
	@Id
	private String id;

	public Telco() {
	}

	public Telco(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public interface Field {
		java.lang.reflect.Field ID = FieldFinder.get(Telco.class, "id");
	}
}
