package com.acme.app.topup.telco.offering.service.create;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.service.AbstractServiceTester;
import com.acme.app.topup.validation.ExtendedValidationException;

@SpringBootTest(classes = { TestCreateOfferingServiceImpl.class })
public class TestCreateOfferingServiceImpl extends AbstractServiceTester {
	private static final Telco TELCO = new Telco("TELCO");
	private static final String TURBO_OFFER = "TURBO";

	private CreateOfferingServiceImpl createOfferingServiceImpl;
	
	@Before
	public void setup() {
		this.createOfferingServiceImpl = new CreateOfferingServiceImpl(offeringDAO);
	}
	
	@Test
	public void testMissingTelco() {
		try {
			this.createOfferingServiceImpl.execute(null, TURBO_OFFER);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, Offering.class.getName(), "owner", null);
		}
	}

	@Test
	public void testMissingOffering() {
		try {
			this.createOfferingServiceImpl.execute(TELCO, null);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, Offering.class.getName(), "externalId", null);
		}
	}

	@Test
	public void testSuccessCreation() {
		try {
			Offering offering = this.createOfferingServiceImpl.execute(TELCO, TURBO_OFFER);
			assertNotNull(offering);
			checkOffering(offering, TELCO.getId(), TURBO_OFFER, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testDuplicateOffering() {
		try {

			Offering offering = this.createOfferingServiceImpl.execute(TELCO, TURBO_OFFER);
			assertNotNull(offering);
			checkOffering(offering, TELCO.getId(), TURBO_OFFER, Offering.Status.ACTIVE);

			offering = this.createOfferingServiceImpl.execute(TELCO, TURBO_OFFER);
			fail("Expected ValitationException");
		} catch (ExtendedValidationException e) {
			checkValidation(e, Offering.class.getName(), "externalId", TURBO_OFFER);
		} catch (Throwable e) {
			e.printStackTrace();
			fail("Not Expected ValitationException");
		}
	}
}
