package com.acme.app.topup.telco.service.find;

import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.model.dao.TelcoDAO;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.NotFoundValidationException;

@Named
public class GetTelcoServiceImpl implements GetTelcoService {

	private final TelcoDAO telcoDAO;
	
	public GetTelcoServiceImpl(TelcoDAO telcoDAO) {
		super();
		Assert.notNull(telcoDAO, "TelcoDAO cannot be null");
		this.telcoDAO = telcoDAO;
	}

	@Override
	public Telco execute(String id) throws ExtendedValidationException {
		Assert.mandatory(Telco.Field.ID, id);
		Telco result = this.telcoDAO.findOne(id);
		if (null == result) {
			throw new NotFoundValidationException(Telco.class, id);
		}
		
		return result;
	}

}
