package com.acme.app.topup.util;

import java.lang.reflect.Field;

import com.acme.app.topup.validation.Assert;

public class FieldFinder {

	private FieldFinder() {
	}

	public static Field get(Class<?> tClass, String fieldName) {
		Assert.notNull(tClass, "Target Class cannot be null");
		Assert.notNull(fieldName, "Field name cannot be null");
		try {
			return tClass.getDeclaredField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			throw new IllegalArgumentException(String.format("Field [%s] not found in class %s", fieldName, tClass));
		}
	}
}
