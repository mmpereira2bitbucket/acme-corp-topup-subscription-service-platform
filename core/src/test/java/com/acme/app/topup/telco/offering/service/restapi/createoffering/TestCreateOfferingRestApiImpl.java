package com.acme.app.topup.telco.offering.service.restapi.createoffering;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.model.dao.TelcoDAO;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingCostService;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingService;
import com.acme.app.topup.telco.offering.service.create.TestCreateOfferingServiceImpl;
import com.acme.app.topup.telco.offering.service.create.restapi.CreateOfferingRestApi;
import com.acme.app.topup.telco.offering.service.create.restapi.CreateOfferingRestApiImpl;
import com.acme.app.topup.telco.service.find.GetTelcoService;
import com.acme.app.topup.validation.ExtendedValidationException;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = { TestCreateOfferingServiceImpl.class })
@ComponentScan(basePackages = { "com.acme.app.topup.telco.offering" })
@EntityScan(basePackageClasses = { Offering.class, Telco.class })
@EnableJpaRepositories(basePackages = { "com.acme.app.topup" }) // assuming you
// @EnableJpaRepositories(basePackageClasses = OfferingDAO.class) // assuming
// you
// have all the
// spring data
// repo in the
// same packag
public class TestCreateOfferingRestApiImpl {
	private static final String TELCO = "TELCO";
	private static final String TURBO_OFFER = "TURBO";
	private static final String YESTERDAY = LocalDate.now().minus(1, ChronoUnit.DAYS)
			.format(DateTimeFormatter.BASIC_ISO_DATE);

	@Inject
	private CreateOfferingService createOfferingService;
	@Inject
	private CreateOfferingCostService createOfferingCostService;
	@Inject
	private GetTelcoService getTelcoService;
	@Inject
	private TelcoDAO telcoDAO;

	@Test
	public void testSuccessInvocation() {
		try {
			assertNotNull(telcoDAO.save(new Telco(TELCO)));
			CreateOfferingRestApi.Input input = new CreateOfferingRestApi.Input();
			input.offeringId = TURBO_OFFER;
			input.validFrom = YESTERDAY;
			input.cost = 10.00;

			new CreateOfferingRestApiImpl(createOfferingService, getTelcoService, createOfferingCostService)
					.createOffering(TELCO, input);

		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
}
