package com.acme.app.topup.validation;

import java.lang.reflect.Field;

import com.acme.app.topup.validation.ExtendedValidationException.ValidationType;

public class Assert {
	public interface MessageFactory {
		String create();
	}

	public interface ValidationExceptionFactory {
		ExtendedValidationException createValidationException();
	}

	static public <T> T notNull(T object, String msg) {
		if (null == object) {
			throw new NullPointerException(msg);
		}

		return object;
	}

	static public <T> T notNull(T object, MessageFactory msg) {
		if (null == object) {
			throw new NullPointerException(msg.create());
		}

		return object;
	}

	static public <T> T notNull(T object, ValidationExceptionFactory factory) throws ExtendedValidationException {
		if (null == object) {
			throw factory.createValidationException();
		}

		return object;
	}

	static public <T> T mandatory(Field field, T value) throws ExtendedValidationException {
		return validate(field, value, null != value, ValidationType.MANDATORY);
	}

	static public <T> T found(Field field, T value) throws ExtendedValidationException {
		return validate(field, value, null != value, ValidationType.NOT_FOUND);
	}

	static public <P, V> P enumeration(Field field, P[] permittedValues, Object rejectedValue)
			throws ExtendedValidationException {
		if (null != rejectedValue) {
			String rejectedAsString = rejectedValue.toString();
			for (P constant : permittedValues) {
				String constStr = constant.toString();
				if (constStr.equals(rejectedAsString) || constStr.equals(rejectedAsString.toLowerCase())
						|| constStr.equals(rejectedAsString.toUpperCase())) {
					return constant;
				}
			}
		}

		throw new ExtendedValidationException(field, rejectedValue, ValidationType.PERMITTED_VALUE, new Object[] {permittedValues});
	}

	static private <T> T validate(Field field, T value, boolean condition, ValidationType type)
			throws ExtendedValidationException {
		if (condition == false) {
			throw new ExtendedValidationException(field, value, type);
		}

		return value;
	}

	static public void isTrue(boolean condition, MessageFactory msg) throws ExtendedValidationException {
		if (!condition) {
			throw new ExtendedValidationException(msg.create());
		}
	}

	// static public void isTrue(boolean condition, FieldErrorFactory factory)
	// throws ValidationException {
	// if (!condition) {
	// throw new ValidationException(factory.createFieldError());
	// }
	// }

	static public <T> T isValid(T entry) throws ExtendedValidationException {
		@SuppressWarnings("unchecked")
		Class<T> targetClass = (Class<T>) entry.getClass();
		return InstanceValidator.createFor(targetClass).validate(entry);
	}
}
