package com.acme.app.topup.telco.offering.service.restapi;

import java.util.List;

import javax.inject.Inject;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.model.dao.TelcoDAO;
import com.acme.app.topup.telco.offering.service.change.restapi.ChangeOfferingRestApi;
import com.acme.app.topup.telco.offering.service.create.restapi.CreateOfferingRestApi;
import com.acme.app.topup.telco.offering.service.find.restapi.FindOfferingRestApi;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;

@RestController
@RequestMapping(value = "/{telcoId}")
public class OfferingRestApiController implements CreateOfferingRestApi, ChangeOfferingRestApi, FindOfferingRestApi {

	private final CreateOfferingRestApi createOfferingRestApi;
	private final ChangeOfferingRestApi cancelOfferingRestApi;
	private final FindOfferingRestApi findOfferingRestApi;

	@Inject
	public OfferingRestApiController(CreateOfferingRestApi createOfferingRestApi,
			ChangeOfferingRestApi cancelOfferingRestApi, FindOfferingRestApi findOfferingRestApi, TelcoDAO telcoDAO) {
		super();
		Assert.notNull(cancelOfferingRestApi, "cancelOfferingRestApi cannot be null");
		Assert.notNull(createOfferingRestApi, "createOfferingRestApi cannot be null");
		Assert.notNull(findOfferingRestApi, "FindOfferingRestApi cannot be null");
		this.cancelOfferingRestApi = cancelOfferingRestApi;
		this.createOfferingRestApi = createOfferingRestApi;
		this.findOfferingRestApi = findOfferingRestApi;

		telcoDAO.save(new Telco("telco"));
	}

	@EventSource
	@Transactional
	@Override
	public ChangeOfferingRestApi.Output changeOffering(@PathVariable("telcoId") String telcoId,
			@PathVariable("id") String offeringId, @RequestBody ChangeOfferingRestApi.Input input)
			throws ExtendedValidationException {
		return cancelOfferingRestApi.changeOffering(telcoId, offeringId, input);
	}

	@EventSource
	@Transactional
	@Override
	public Void createOffering(@PathVariable("telcoId") String telcoId, @RequestBody CreateOfferingRestApi.Input input)
			throws ExtendedValidationException {
		return createOfferingRestApi.createOffering(telcoId, input);
	}

	@EventSource
	@Override
	public FindOfferingRestApi.Output findOneOffering(@PathVariable("telcoId") String telcoId,
			@PathVariable("id") String offeringExternalId, @PathVariable("atDate") String atDate)
			throws ExtendedValidationException {
		return this.findOfferingRestApi.findOneOffering(telcoId, offeringExternalId, atDate);
	}

	@EventSource
	@Override
	public List<FindOfferingRestApi.Output> findOfferings(@PathVariable("telcoId") String telcoId,
			@PathVariable("atDate") String atDate) throws ExtendedValidationException {
		return this.findOfferingRestApi.findOfferings(telcoId, atDate);
	}

	@EventSource
	@Override
	public List<FindOfferingRestApi.Output> findOfferings(@PathVariable("telcoId") String telcoId)
			throws ExtendedValidationException {
		return this.findOfferingRestApi.findOfferings(telcoId);
	}

	@EventSource
	@Override
	public FindOfferingRestApi.Output findOneOffering(@PathVariable("telcoId") String telcoId, @PathVariable("id") String offeringExternalId)
			throws ExtendedValidationException {
		return this.findOfferingRestApi.findOneOffering(telcoId, offeringExternalId);
	}
}
