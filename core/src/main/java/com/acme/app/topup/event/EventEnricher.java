package com.acme.app.topup.event;

/**
 * EventEnricher add more information to the event to be further processed
 * by listeners
 */
public interface EventEnricher {
	/**
	 * Invoked before the method be executed
	 * @param event
	 */
	void onCreation(Event event);
	/**
	 * Invoked after the method be executed with success
	 * @param event
	 * @param method result
	 */
	void onSuccess(Event event, Object methodResult);
	/**
	 * Invoked after an exception stopping the method execution
	 * @param event
	 */
	void onException(Event event, Throwable failure);
}