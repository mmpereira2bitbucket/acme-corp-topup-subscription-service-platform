package com.acme.app.topup.telco.offering.service.change.restapi;

import java.time.LocalDate;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.offering.service.change.status.CancelOfferingService;
import com.acme.app.topup.telco.offering.service.change.status.InativateOfferingService;
import com.acme.app.topup.telco.offering.service.change.status.ReativateOfferingService;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingCostService;
import com.acme.app.topup.telco.offering.service.find.FindOfferingCostService;
import com.acme.app.topup.telco.offering.service.find.FindOfferingService;
import com.acme.app.topup.telco.service.find.GetTelcoService;
import com.acme.app.topup.util.Converter;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;

@Named
public class ChangeOfferingRestApiImpl implements ChangeOfferingRestApi {

	private final GetTelcoService getTelcoService;
	private final FindOfferingService findOfferingService;
	private final FindOfferingCostService findOfferingCostService;
	private final CancelOfferingService cancelOfferingService;
	private final InativateOfferingService inativateOfferingService;
	private final ReativateOfferingService reativateOfferingService;
	private final CreateOfferingCostService createOfferingCostService;

	@Inject
	public ChangeOfferingRestApiImpl(GetTelcoService getTelcoService, FindOfferingService findOfferingService,
			CancelOfferingService cancelOfferingService, InativateOfferingService inativateOfferingService,
			ReativateOfferingService reativateOfferingService, CreateOfferingCostService createOfferingCostService,
			FindOfferingCostService findOfferingCostService) {
		super();
		Assert.notNull(cancelOfferingService, "CancelOfferingService cannot be null");
		Assert.notNull(findOfferingService, "FindOfferingService cannot be null");
		Assert.notNull(getTelcoService, "FindTelcoService cannot be null");
		Assert.notNull(inativateOfferingService, "InativateOfferingService cannot be null");
		Assert.notNull(reativateOfferingService, "ReativateOfferingService cannot be null");
		Assert.notNull(createOfferingCostService, "CreateOfferingCostService cannot be null");
		this.cancelOfferingService = cancelOfferingService;
		this.findOfferingService = findOfferingService;
		this.getTelcoService = getTelcoService;
		this.inativateOfferingService = inativateOfferingService;
		this.reativateOfferingService = reativateOfferingService;
		this.createOfferingCostService = createOfferingCostService;
		this.findOfferingCostService = findOfferingCostService;
	}

	@Override
	public Output changeOffering(String telcoId, String offeringExternalId, Input input)
			throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telcoId);
		Assert.mandatory(Offering.Field.EXTERNAL_ID, offeringExternalId);

		OfferingCost offeringCost = null;
		Telco telco = this.getTelcoService.execute(telcoId);
		Offering offering = this.findOfferingService.execute(telco, offeringExternalId);
		if (null != input.status) {
			Offering.Status status = Assert.enumeration(Offering.Field.STATUS, Offering.Status.values(), input.status);
			if (offering.getStatus() != status) {
				if (offering.getStatus() == Offering.Status.CANCELLED) {
					throw new ExtendedValidationException(Offering.Field.EXTERNAL_ID, offeringExternalId,
							"CancelledOfferingBeingRemoved");
				}
				switch (status) {
				case CANCELLED:
					offering = this.cancelOfferingService.execute(offering);
					break;
				case INACTIVE:

					offering = this.inativateOfferingService.execute(offering);
					break;
				case ACTIVE:
					offering = this.reativateOfferingService.execute(offering);
					break;
				}
			}
		}

		if (null != input.cost) {
			LocalDate validFrom = Converter.fromYYYYMMDD2LocalDate(OfferingCost.Field.VALID_FROM, input.validFrom);
			LocalDate validUntil = Converter.fromYYYYMMDD2LocalDate(OfferingCost.Field.VALID_UNTIL, input.validUntil);
			offeringCost = this.createOfferingCostService.execute(offering, validFrom, validUntil, input.cost);
		}
		else {
			offeringCost = this.findOfferingCostService.execute(offering, LocalDate.now());
		}

		Output output = new Output();
		output.offeringId = offering.getExternalId();
		output.status = offering.getStatus().name().toLowerCase();
		if (null != offeringCost) {
			output.cost = offeringCost.getCost();
			output.validFrom = Converter.fromDate2YYYYMMDD(OfferingCost.Field.VALID_FROM, offeringCost.getValidFrom());
			output.validUntil = Converter.fromDate2YYYYMMDD(OfferingCost.Field.VALID_UNTIL, offeringCost.getValidUntil());
		}

		return output;
	}
}
