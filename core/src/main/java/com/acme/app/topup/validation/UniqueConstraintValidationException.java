package com.acme.app.topup.validation;

public class UniqueConstraintValidationException extends ExtendedValidationException {

	public UniqueConstraintValidationException(Class<?> entityClass, String fieldName, Object dupValue) {
		this(entityClass, fieldName, dupValue, null, null);
	}

	public UniqueConstraintValidationException(Class<?> entityClass, String fieldName, Object dupValue,
			Object[] messageKeyArgs) {
		this(entityClass, fieldName, dupValue, messageKeyArgs, null);
	}

	public UniqueConstraintValidationException(Class<?> entityClass, String fieldName, Object dupValue,
			Object[] messageKeyArgs, Throwable cause) {
		super(entityClass, fieldName, dupValue, ValidationType.DUPLICATED, messageKeyArgs);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3338330720972833879L;

}
