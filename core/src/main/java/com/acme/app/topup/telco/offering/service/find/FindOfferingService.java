package com.acme.app.topup.telco.offering.service.find;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.service.Service;
import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.validation.ExtendedValidationException;

/**
 * Find a given offering
 */
public interface FindOfferingService extends Service {
	
	@EventSource
	Offering execute(Telco telco, String offeringExternalId) throws ExtendedValidationException;
}
