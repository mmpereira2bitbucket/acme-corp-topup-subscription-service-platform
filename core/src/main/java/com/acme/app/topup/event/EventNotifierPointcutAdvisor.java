package com.acme.app.topup.event;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.inject.Inject;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.stereotype.Component;

@Component
public class EventNotifierPointcutAdvisor extends AbstractPointcutAdvisor {

	static protected Method getConformingMethod(String methodName, Class<?>[] actualParameters, Class<?> cls) {
		Method[] publicMethods = cls.getMethods();
		Method m = null;
		int idxMethod = 0;
		while ((m == null) && (idxMethod < publicMethods.length)) {
			m = publicMethods[idxMethod];
			if (m.getName().equals(methodName)) {
				Class<?>[] formalParameters = m.getParameterTypes();
				if (actualParameters.length == formalParameters.length) {
					int idxParam = 0;
					while ((m != null) && (idxParam < formalParameters.length)) {
						Class<?> param = formalParameters[idxParam];
						if (!param.isAssignableFrom(actualParameters[idxParam])) {
							m = null;
						}
						idxParam++;
					}
				} else {
					m = null;
				}
			} else {
				m = null;
			}
			idxMethod++;
		}
		return m;
	}

	static private Method getEventSource(Class<? extends Annotation> aClass, Method m, Class<?>[] actualParameters) {
		Method result = null;
		if (m != null) {
			if (m.getAnnotation(aClass) == null) {
				Class<?> parent = m.getDeclaringClass().getSuperclass();
				if (parent != null && parent != Object.class) {
					Method superMethod = getConformingMethod(m.getName(), actualParameters, parent);
					result = getEventSource(aClass, superMethod, actualParameters);
				}
				if (null == result) {
					Class<?>[] iList = m.getDeclaringClass().getInterfaces();
					if (iList != null) {
						for (int i = 0; i < iList.length && result == null; i++) {
							Method superMethod = getConformingMethod(m.getName(), actualParameters, iList[i]);
							result = getEventSource(aClass, superMethod, actualParameters);
						}
					}
				}
			}
			else {
				result = m;
			}
		}
		return result;
	}

	private static final long serialVersionUID = 1L;

    private final StaticMethodMatcherPointcut pointcut = new
            StaticMethodMatcherPointcut() {
                @Override
                public boolean matches(Method method, Class<?> targetClass) {
                	if (method.isSynthetic()) {
                		return false;
                	}
                	
                	boolean found = EventManager.getInstance().isEventSourceRegistered(method);
                	if (!found) {
	                	Method methodWithAnnotation = getEventSource(EventSource.class, method, method.getParameterTypes());
	                	found = null != methodWithAnnotation;
	                	if (found) {
	                		EventManager.getInstance().registerEventSource(method, methodWithAnnotation, targetClass);
	                	}
                	}
                	
                    return found;
                }
            };

    @Inject
    private EventSourceMethodInterceptor interceptor;

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.interceptor;
    }
} 