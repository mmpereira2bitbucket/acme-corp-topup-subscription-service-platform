package com.acme.app.topup.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.acme.app.topup.util.MessageKeyBuilder;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class NotFoundValidationException extends ExtendedValidationException {
	
	public NotFoundValidationException(Class<?> tClass, String notFoundValue) {
		super(MessageKeyBuilder.build(tClass, notFoundValue, ValidationType.NOT_FOUND.name()));
	}

	public NotFoundValidationException(Class<?> tClass, String notFoundValue, String constraint) {
		super(MessageKeyBuilder.build(tClass, notFoundValue, constraint));
	}
}
