package com.acme.app.topup.telco.offering.model.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.acme.app.topup.telco.offering.model.OfferingCost;

@Repository
public interface OfferingCostDAO extends CrudRepository<OfferingCost, Long> {
	@Query("select c from OfferingCost c join c.offering o "
			+ "where o.externalId = ?1 "
			+ "and o.status != 2 "
			+ "and ("
			+ "     ("
			+ "			?2 is null and "
			+ "			("
			+ "				c.validUntil is NULL or "
			+ "				c.validUntil = (select max(c.validUntil) from OfferingCost c join c.offering o where o.externalId = ?1)"
			+ "     	)"
			+ "     )"
			+ "     or "
			+ "     ("
			+ "     	(?2 >= c.validFrom and ?2 <= c.validUntil) or "
			+ "         (?2 >= c.validFrom and c.validUntil is NULL)"
			+ "      )"
			+ "    ))"
			)
	List<OfferingCost> searchCostFor(String offeringExternalId, Date date);

	@Query("select c from OfferingCost c join c.offering o "
			+ "where o.id = ?1 "
			+ "and ("
			+ "     ("
			+ "			?2 is null and "
			+ "			("
			+ "				c.validUntil is NULL or "
			+ "				c.validUntil = (select max(c.validUntil) from OfferingCost c join c.offering o where o.id = ?1)"
			+ "     	)"
			+ "     )"
			+ "     or "
			+ "     ("
			+ "     	(?2 >= c.validFrom and ?2 <= c.validUntil) or "
			+ "         (?2 >= c.validFrom and c.validUntil is NULL)"
			+ "      )"
			+ "    ))"
			)
	List<OfferingCost> findActiveCostsForDateUsingId(long id, Date date);

	@Query("select c from OfferingCost c join c.offering o join o.owner t "
			+ "where t.id = ?1 "
			+ "and ("
			+ "     ("
			+ "			?2 is null and "
			+ "			("
			+ "				c.validUntil is NULL or "
			+ "				c.validUntil = (select max(c.validUntil) from OfferingCost c join c.offering o where o.id = ?1)"
			+ "     	)"
			+ "     )"
			+ "     or "
			+ "     ("
			+ "     	(?2 >= c.validFrom and ?2 <= c.validUntil) or "
			+ "         (?2 >= c.validFrom and c.validUntil is NULL)"
			+ "      )"
			+ "    ))"
			)
	List<OfferingCost> findCostsUsingTelcoId(String telcoId, Date date);
	
	@Query("select c from OfferingCost c join c.offering o where o.id = ?1")
	List<OfferingCost> findCostsUsingOfferingId(long id);
}
