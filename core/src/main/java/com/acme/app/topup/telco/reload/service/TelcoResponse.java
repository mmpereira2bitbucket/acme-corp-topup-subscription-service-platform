package com.acme.app.topup.telco.reload.service;

/**
 * Basic information returned for all request to Telco services.
 */
public class TelcoResponse {

	private final String id;
	
	public TelcoResponse(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
