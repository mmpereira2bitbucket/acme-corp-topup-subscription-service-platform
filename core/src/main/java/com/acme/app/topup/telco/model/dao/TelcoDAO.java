package com.acme.app.topup.telco.model.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.acme.app.topup.telco.model.Telco;

@Repository
public interface TelcoDAO extends CrudRepository<Telco, String> {

}
