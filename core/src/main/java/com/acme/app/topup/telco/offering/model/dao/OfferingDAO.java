package com.acme.app.topup.telco.offering.model.dao;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.acme.app.topup.telco.offering.model.Offering;

@Repository
public interface OfferingDAO extends CrudRepository<Offering, Long> {
	@Query("select o from Offering o join o.owner t where o.externalId = ?2 and t.id = ?1")
	Offering findByExternalId(String telcoid, String externalId);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query ("select o from Offering o where o.id = ?1")
	Offering lockForUpdate(long id);

	@Query("select o from Offering o join o.owner t where t.id = ?1")
	List<Offering> findAllByTelco(String id);
}
