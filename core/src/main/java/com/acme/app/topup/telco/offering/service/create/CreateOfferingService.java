package com.acme.app.topup.telco.offering.service.create;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.service.Service;
import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.validation.ExtendedValidationException;

/**
 * Register a new offering in the Topup system.
 *
 */
public interface CreateOfferingService extends Service {
	@EventSource
	@Transactional(propagation = Propagation.REQUIRED)
	Offering execute(Telco telco, String offeringExternalId) throws ExtendedValidationException;
}
