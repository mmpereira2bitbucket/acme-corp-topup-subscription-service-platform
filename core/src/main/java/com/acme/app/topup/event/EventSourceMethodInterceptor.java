package com.acme.app.topup.event;

import javax.inject.Named;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

@Named
public class EventSourceMethodInterceptor implements MethodInterceptor  {
	
	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		Event event = EventManager.getInstance().createEvent(methodInvocation.getMethod());
		
		try {
			Object result = methodInvocation.proceed();
			EventManager.getInstance().notifySuccess(event, result);
			return result;
		} catch (Throwable e) {
			EventManager.getInstance().notifyFailure(event, e);
			throw e;
		}
	}
}
