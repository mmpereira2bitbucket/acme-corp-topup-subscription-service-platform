package com.acme.app.topup.telco.offering.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.util.FieldFinder;

/**
 * Offering is an entity that represents an offer provided by TELCO
 */
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "telco_id", "externalId" }))
@Entity
public class Offering {

	public enum Status {
		/*
		 * An offering is active when new subscription can be created with it.
		 */
		ACTIVE,
		/*
		 * An offering is inactive when it is no longer allowed creating new
		 * subscription with it, but new recurrences can be performed.
		 */
		INACTIVE,
		/*
		 * An offering is cancelled when it is neither allowed to create new
		 * subscription with the offering nor to perform recurrences with it. It
		 * is derived
		 */
		CANCELLED
	};

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * TELCO that provides this offering
	 */
	@JoinColumn(name = "telco_id")
	@Basic(optional = false)
	@ManyToOne(cascade = CascadeType.ALL)
	private Telco owner = null;

	/**
	 * External identification is how TELCO identifies the offering
	 */
	@NotNull
	@NotEmpty
	@Size(max=10)
	private String externalId = null;

	/**
	 * Status of the offering
	 */
	@Enumerated(EnumType.ORDINAL)
	private Status status = Status.ACTIVE;

	public long getId() {
		return id;
	}

	public Telco getOwner() {
		return owner;
	}

	public void setOwner(Telco owner) {
		this.owner = owner;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public interface Field {
		java.lang.reflect.Field ID = FieldFinder.get(Offering.class, "id");
		java.lang.reflect.Field OWNER = FieldFinder.get(Offering.class, "owner");
		java.lang.reflect.Field EXTERNAL_ID = FieldFinder.get(Offering.class, "externalId");
		java.lang.reflect.Field STATUS = FieldFinder.get(Offering.class, "status");

	}
}
