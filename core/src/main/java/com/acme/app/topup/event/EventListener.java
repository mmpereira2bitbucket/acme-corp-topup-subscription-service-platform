package com.acme.app.topup.event;

/**
 * EventListener are notified when events are published
 */
public interface EventListener {
	void onEvent(Event event);
}