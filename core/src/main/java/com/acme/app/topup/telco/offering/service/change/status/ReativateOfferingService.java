package com.acme.app.topup.telco.offering.service.change.status;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.validation.ExtendedValidationException;

/**
 * ReativateOfferingService changes the offering status to active if it is inactive, or active.
 */
public interface ReativateOfferingService {
	@EventSource
	@Transactional(propagation=Propagation.REQUIRED)
	Offering execute(Offering offering) throws ExtendedValidationException;
}
