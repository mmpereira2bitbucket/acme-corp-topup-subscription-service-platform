package com.acme.app.topup.telco.offering.service.create.restapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.acme.app.topup.service.restapi.RestApi;
import com.acme.app.topup.validation.ExtendedValidationException;

/**
 * Register a new offering in the Topup system.
 *
 */
public interface CreateOfferingRestApi extends RestApi {

	class Input {
		public String offeringId = null;
		public String validFrom = null; // YYYYMMDD
		public String validUntil = null; // YYYYMMDD
		public Double cost = null;

	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "offering", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public Void createOffering(@PathVariable("telcoId") String telcoId, @RequestBody CreateOfferingRestApi.Input input)
			throws ExtendedValidationException;
}
