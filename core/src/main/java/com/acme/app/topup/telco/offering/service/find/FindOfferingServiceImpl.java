package com.acme.app.topup.telco.offering.service.find;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.dao.OfferingDAO;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;

@Named
public class FindOfferingServiceImpl implements FindOfferingService {

	private final OfferingDAO offeringDAO;

	@Inject
	public FindOfferingServiceImpl(OfferingDAO offeringDAO) {
		super();
		Assert.notNull(offeringDAO, "OfferingDAO cannot be null");
		this.offeringDAO = offeringDAO;
	}

	@Override
	public Offering execute(Telco telco, String offeringExternalId) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telco);
		Assert.mandatory(Offering.Field.OWNER, telco.getId());
		Assert.mandatory(Offering.Field.EXTERNAL_ID, offeringExternalId);
		return Assert.found(Offering.Field.EXTERNAL_ID, this.offeringDAO.findByExternalId(telco.getId(), offeringExternalId));
	}

}
