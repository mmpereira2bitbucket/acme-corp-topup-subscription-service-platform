package com.acme.app.topup.telco.offering.service.create;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.dao.OfferingDAO;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.ExtendedValidationException.ValidationType;

@Named
public class CreateOfferingServiceImpl implements CreateOfferingService {

	private final OfferingDAO offeringDAO;
	
	@Inject
	public CreateOfferingServiceImpl(OfferingDAO offeringDAO) {
		super();
		Assert.notNull(offeringDAO, () -> "OfferingDAO cannot be null");
		this.offeringDAO = offeringDAO;
	}
	
	@Override
	public Offering execute(Telco telco, String offeringExternalId) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telco);
		Assert.mandatory(Offering.Field.OWNER, telco.getId());
		Assert.mandatory(Offering.Field.EXTERNAL_ID, offeringExternalId);

		Offering offering = offeringDAO.findByExternalId(telco.getId(), offeringExternalId);
		if (null != offering) {
			if (offering.getStatus() == Offering.Status.CANCELLED) {
				throw new ExtendedValidationException(Offering.class, "externalId", offeringExternalId, "CancelledOfferingBeingRemoved");
			}
			
			throw new ExtendedValidationException(Offering.class, "externalId", offeringExternalId, ValidationType.DUPLICATED);
		}
		
		offering = new Offering();
		offering.setExternalId(offeringExternalId.toUpperCase());
		offering.setOwner(telco);
		offering.setStatus(Offering.Status.ACTIVE);
		return this.offeringDAO.save(offering);
	}
}
