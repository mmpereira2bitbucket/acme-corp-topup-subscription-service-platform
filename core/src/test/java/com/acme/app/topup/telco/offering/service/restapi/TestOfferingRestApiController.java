package com.acme.app.topup.telco.offering.service.restapi;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionStatus;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.model.dao.TelcoDAO;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.service.change.restapi.ChangeOfferingRestApi;
import com.acme.app.topup.telco.offering.service.create.restapi.CreateOfferingRestApi;
import com.acme.app.topup.telco.service.AbstractTester;

@TestPropertySource(properties = { "security.basic.enabled = false" })
@RunWith(SpringRunner.class)
@ComponentScan(basePackages = { "com.acme.app.topup" })
@EntityScan(basePackageClasses = { Offering.class, Telco.class })
@EnableJpaRepositories(basePackages = { "com.acme.app.topup" })
// @WebMvcTest(OfferingRestApiController.class)
@SpringBootTest(classes = OfferingRestApiController.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })
public class TestOfferingRestApiController extends AbstractTester {
	private static final String TELCO = "TELCO";
	private static final String API_CREATE_OFFERING = "/" + TELCO + "/offering";
	private static final String TURBO_OFFER = "TURBO";
	private static final String API_CHANGE_OFFERING = "/" + TELCO + "/offering/" + TURBO_OFFER;
	private static final String TODAY_STR = LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE);
	private static final LocalDate TODAY = LocalDate.now();
	private static final LocalDate YESTERDAY = TODAY.minus(1, ChronoUnit.DAYS);
	private static final Double COST_1 = 10.43;

	@Inject
	public TestRestTemplate restTemplate;

	@Inject
	private TelcoDAO telcoDAO;

	@Test
	public void testCreateOffering() {
		ApiTester<CreateOfferingRestApi.Input, Void> createOfferingRestApi = new ApiTester<>(
				this.restTemplate, HttpMethod.POST, API_CREATE_OFFERING, Void.class);

		try {
			TransactionStatus transaction = this.createTransaction();
			telcoDAO.save(new Telco(TELCO));
			this.commitTransaction(transaction);
	
			CreateOfferingRestApi.Input input = new CreateOfferingRestApi.Input();
			input.offeringId = TURBO_OFFER;
			input.validFrom = TODAY_STR;
			input.cost = 10.00;
			createOfferingRestApi.whenInvokingisHttpStatus(input, HttpStatus.CREATED);
		}
		finally {
			TransactionStatus transaction = this.createTransaction();
			this.deleteCascadeTelco(new Telco(TELCO));
			this.commitTransaction(transaction);
		}
	}
	
	@Test
	public void testCancelOferring() {
		ApiTester<ChangeOfferingRestApi.Input, Void> changeOfferingRestApi = new ApiTester<>(
				this.restTemplate, HttpMethod.PATCH, API_CHANGE_OFFERING, Void.class);

		try {
			/*
			 * Create context for test
			 */
			TransactionStatus transaction = this.createTransaction();
			Telco telco = telcoDAO.save(new Telco(TELCO));
			Offering offering = createOffering(telco, TURBO_OFFER);
			createOfferingCost(offering, YESTERDAY, null, COST_1);
			this.commitTransaction(transaction);
			assertNotNull(telcoDAO.findOne(TELCO));
			
			ChangeOfferingRestApi.Input input = new ChangeOfferingRestApi.Input();
			input.status = "cancelled";
			changeOfferingRestApi.whenInvokingisHttpStatus(input, HttpStatus.OK);
		}
		finally {
			TransactionStatus transaction = this.createTransaction();
			this.deleteCascadeTelco(new Telco(TELCO));
			this.commitTransaction(transaction);
		}
	}
}
