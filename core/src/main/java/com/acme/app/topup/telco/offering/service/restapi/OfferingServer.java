package com.acme.app.topup.telco.offering.service.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.acme.app.topup" })
@EntityScan(basePackages = { "com.acme.app.topup" })
@EnableJpaRepositories(basePackages = { "com.acme.app.topup" })
public class OfferingServer {

	public static void main(String[] args) {
		SpringApplication.run(OfferingServer.class, args);
	}
	
}
