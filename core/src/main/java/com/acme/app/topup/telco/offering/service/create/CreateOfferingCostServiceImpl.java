package com.acme.app.topup.telco.offering.service.create;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.offering.model.dao.OfferingCostDAO;
import com.acme.app.topup.telco.offering.model.dao.OfferingDAO;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.ExtendedValidationException.ValidationType;

@Named
public class CreateOfferingCostServiceImpl implements CreateOfferingCostService {

	private final OfferingDAO offeringDAO;
	private final OfferingCostDAO offeringCostDAO;
	
	@Inject
	public CreateOfferingCostServiceImpl(OfferingDAO offeringDAO, OfferingCostDAO offeringCostDAO) {
		super();
		Assert.notNull(offeringCostDAO, "OfferingCostDAO cannot be null");
		Assert.notNull(offeringDAO, "OfferingDAO cannot be null");
		this.offeringDAO = offeringDAO;
		this.offeringCostDAO = offeringCostDAO;
	}

	@Override
	public OfferingCost execute(Offering offering, LocalDate validFrom, LocalDate validUntil, double cost)
			throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.ID, offering);
		Assert.mandatory(Offering.Field.ID, offering.getId());
		Assert.mandatory(OfferingCost.Field.VALID_FROM, validFrom);
		if (null != validUntil) {
			if (validFrom.compareTo(validUntil) > 0) {
				throw new ExtendedValidationException(OfferingCost.Field.VALID_UNTIL, validUntil, "validFromGreaterThanValidUntil",
						new Object[] { validFrom, validUntil });
			} else {
				LocalDate today = LocalDate.now();
				if (validUntil.compareTo(today) < 0) {
					throw new ExtendedValidationException(OfferingCost.Field.VALID_UNTIL, validUntil, "validUntilGreaterThanToday",
							new Object[] { validUntil, today });
				}
			}
		}
		if (cost <= 0) {
			throw new ExtendedValidationException(OfferingCost.Field.COST, cost, "costLesserOrEqualsZero");
		}

		offering = this.offeringDAO.lockForUpdate(offering.getId());
		/*
		 * At most one object can be found that has the valid cost for the given period that contains validFrom
		 */
		Date validFromDate = Date.valueOf(validFrom);
		List<OfferingCost> existentCosts = this.offeringCostDAO.findActiveCostsForDateUsingId(offering.getId(), validFromDate);
		if (null != existentCosts && existentCosts.size() > 0) {
			int periodsFound = existentCosts.size();
			if (periodsFound > 1) {
				Date[] values = new Date[periodsFound];
				for (int i = 0; i < periodsFound; i++) {
					values[i] = existentCosts.get(i).getValidFrom();
				}
				
				throw new ExtendedValidationException(OfferingCost.Field.VALID_FROM, validFrom, ValidationType.DUPLICATED, values);
			}
			
			OfferingCost lastCost = existentCosts.get(0);
			/*
			 * Check whether the period found is the same to be inserted that it is a violation
			 */
			if (validFromDate.equals(lastCost.getValidFrom())) {
				boolean duplicated = false;
				Date lastCostValidUntil = lastCost.getValidUntil();
				if (lastCostValidUntil == null) {
					if (validUntil == null) {
						duplicated = true;
					}
				}
				else if (validUntil != null && lastCostValidUntil.equals(Date.valueOf(validUntil))) {
					duplicated = true;
				}
				
				if (duplicated) {
					String period = String.format("[%s;%s]", validFrom, validUntil); 
					throw new ExtendedValidationException(OfferingCost.Field.COST, period, ValidationType.DUPLICATED);
				}
			}
			/*
			 * Existent period now ends when the new one starts
			 */
			lastCost.setValidUntil(validFrom.minus(1, ChronoUnit.DAYS));
			this.offeringCostDAO.save(lastCost);
		}

		/*
		 * Create the new cost
		 */
		OfferingCost newCost = new OfferingCost();
		newCost.setCost(cost);
		newCost.setValidFrom(validFrom);
		newCost.setValidUntil(validUntil);
		newCost.setOffering(offering);
		return this.offeringCostDAO.save(newCost);
	}
}
