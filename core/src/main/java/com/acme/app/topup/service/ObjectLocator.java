package com.acme.app.topup.service;

import java.util.HashMap;
import java.util.Map;

import com.acme.app.topup.validation.Assert;

public class ObjectLocator {
	private static ObjectLocator singleton = null;
	
	protected static void setSingleton(ObjectLocator newSingleton) {
		singleton = newSingleton;
	}
	
	public static ObjectLocator getInstance() {
		return Assert.notNull(singleton, () -> "Singleton is null");
	}
	
	private Map<Class<?>, Object> map = new HashMap<Class<?>, Object>();
	
	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> tClass) {
		Assert.notNull(tClass, () -> "Class cannot be null");
		Object result = this.map.get(tClass);
		if (null != result && tClass.isInstance(result)) {
			return (T) result;
		}
		else {
			return null;
		}
	}
	
	public <T> T getNotNull(Class<T> tClass) {
		return Assert.notNull(get(tClass), () -> "No instance of " + tClass + " was located");
	}

	public <T> void register(Class<T> tClass, T object) {
		this.map.put(tClass, object);
	}
}