package com.acme.app.topup.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ExecutionContext implements Map<String, Object> {
	interface Parameters {
		String ExecutionStartTime = "Service.Start";
	}
	
	private static ExecutionContext applicationExecutionContext = new ExecutionContext();
		
	private static ThreadLocal<ExecutionContext> threadExecutionContext  = new ThreadLocal<ExecutionContext>();
	
	public static ExecutionContext getApplicationExecutionContext() {
		return applicationExecutionContext;
	}
	
	public static void setApplicationExecutionContext(ExecutionContext ctx) {
		applicationExecutionContext = ctx;
	}
	
	public static ExecutionContext getThreadExecutionContext() {
		ExecutionContext ctx = threadExecutionContext.get();
		if (null == ctx) {
			ctx = new ExecutionContext();
			threadExecutionContext.set(ctx);
		}
		
		return ctx;
	}
	
	public static void setThreadExecutionContext(ExecutionContext ctx) {
		threadExecutionContext.set(ctx);
	}
	
	private Map<String, Object> map = new HashMap<String, Object>();

	public ExecutionContext() {}
	
	@Override
	public int size() {
		return this.map.size();
	}

	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return this.map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return this.map.containsValue(value);
	}

	@Override
	public Object get(Object key) {
		return this.map.get(key);
	}

	@SuppressWarnings("unchecked")
	public <T> Object getAs(Object key, Class<T> tClass) {
		Object result = this.map.get(key);
		if (null != result && tClass.isInstance(result)) {
			return (T) result;
		}
		else {
			return null;
		}
	}

	public <T> Object get(Class<T> tClass) {
		return getAs(tClass, tClass);
	}

	@Override
	public Object put(String key, Object value) {
		return this.map.put(key, value);
	}

	@Override
	public Object remove(Object key) {
		return this.map.remove(key);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		this.map.putAll(m);
	}

	@Override
	public void clear() {
		this.map.clear();
	}

	@Override
	public Set<String> keySet() {
		return this.map.keySet();
	}

	@Override
	public Collection<Object> values() {
		return this.map.values();
	}

	@Override
	public Set<Map.Entry<String, Object>> entrySet() {
		return this.map.entrySet();
	}
}
