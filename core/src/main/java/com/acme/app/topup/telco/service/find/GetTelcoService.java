package com.acme.app.topup.telco.service.find;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.service.Service;
import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.validation.ExtendedValidationException;

/**
 * Service to return the Telco entity given its id. If it not found a validation
 * exception is thrown.
 * 
 */
public interface GetTelcoService extends Service {

	@EventSource
	Telco execute(String id) throws ExtendedValidationException;
}
