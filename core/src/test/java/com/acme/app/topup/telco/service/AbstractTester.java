package com.acme.app.topup.telco.service;

import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.model.dao.TelcoDAO;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.offering.model.dao.OfferingCostDAO;
import com.acme.app.topup.telco.offering.model.dao.OfferingDAO;
import com.acme.app.topup.telco.offering.service.change.status.CancelOfferingService;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingCostService;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingService;
import com.acme.app.topup.telco.offering.service.find.FindOfferingService;
import com.acme.app.topup.telco.service.find.GetTelcoService;
import com.acme.app.topup.validation.ExtendedValidationException;

public class AbstractTester {

	@Inject
	private GetTelcoService getTelcoService;
	@Inject
	private FindOfferingService findOfferingService;
	@Inject
	private CreateOfferingService createOfferingService;
	@Inject
	private CreateOfferingCostService createOfferingCostService;
	@Inject
	private CancelOfferingService cancelOfferingService;
	@Inject
	protected TelcoDAO telcoDAO;
	@Inject
	protected OfferingDAO offeringDAO;
	@Inject
	protected OfferingCostDAO offeringCostDAO;
	@Inject
	private PlatformTransactionManager transactionManager;
	
	public AbstractTester() {
		super();
	}

	protected Telco getTelco(String id) {
		try {
			return this.getTelcoService.execute(id);
		}
		catch(Throwable e) {
			e.printStackTrace();
			fail("Exception not expect");
			return null;
		}
	}

	protected Offering getOffering(Telco telco, String externalId) {
		try {
			return this.findOfferingService.execute(telco, externalId);
		}
		catch(Throwable e) {
			e.printStackTrace();
			fail("Exception not expect");
			return null;
		}
	}

	protected Offering cancelOffering(Offering offering) {
		try {
			return this.cancelOfferingService.execute(offering);
		}
		catch(Throwable e) {
			e.printStackTrace();
			fail("Exception not expect");
			return null;
		}
	}

	protected Offering createOffering(Telco telco, String offeringId) {
		try {
			return createOfferingService.execute(telco, offeringId);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
			return null;
		}
	}

	protected OfferingCost createOfferingCost(Offering offering, LocalDate validFrom, LocalDate validUntil, Double cost) {
		try {
			return createOfferingCostService.execute(offering, validFrom, validUntil, cost);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
			return null;
		}
	}

	protected void commitTransaction(TransactionStatus transaction) {
		transactionManager.commit(transaction);
	}
    /**
     * Create a new transaction
     * 
     * @return
     * @throws ManagedObjectNotFoundException
     * @throws ManagerException
     */
    protected TransactionStatus createTransaction() {
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        return transactionManager.getTransaction(def);
    }
    
    protected void deleteCascadeTelco(Telco telco) {
    	
    	List<Offering> offeringList = this.offeringDAO.findAllByTelco(telco.getId());
    	for (Offering offering: offeringList) {
    		this.offeringCostDAO.delete(this.offeringCostDAO.findCostsUsingOfferingId(offering.getId()));
    		this.offeringDAO.delete(offering);
    	}
    	this.telcoDAO.delete(telco);
    }
}