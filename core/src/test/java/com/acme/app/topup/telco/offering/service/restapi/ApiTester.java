package com.acme.app.topup.telco.offering.service.restapi;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.ResponseErrorHandler;

public class ApiTester<I,O> {
	private TestRestTemplate restTemplate;

	private class ResponseErrorHandlerImpl implements ResponseErrorHandler {

		public HttpStatus expectedHttpStatus = HttpStatus.OK;
		public String body = null;
				
		@Override
		public boolean hasError(ClientHttpResponse response) throws IOException {
			// TODO Auto-generated method stub
			return expectedHttpStatus != response.getStatusCode();
		}

		@Override
		public void handleError(ClientHttpResponse response) throws IOException {
			if (expectedHttpStatus != response.getStatusCode()) {
				InputStream is = response.getBody();
				StringBuilder sb = new StringBuilder();
				int c;
				while ((c = is.read()) != -1) {
					sb.append((char) c);
				}
				
				body = sb.toString();
			}
		}
		
	}

	public class CheckRestResult {

		private ResponseEntity<O> result;
		private ResponseErrorHandlerImpl errorHandle;
		
		private CheckRestResult(ResponseEntity<O> result, ResponseErrorHandlerImpl errorHandle) {
			this.result = result;
			this.errorHandle = errorHandle;
			
		}

		CheckRestResult isResultCode(HttpStatus status) {
			if (status != this.result.getStatusCode()) {
				System.err.println("Body[" + this.errorHandle.body + "]");
			}

			assertEquals(status, this.result.getStatusCode());
			return this;
		}
	}
	
	private Class<O> outputClass;
	private HttpMethod httpMethod;
	private String uri;
	
	public ApiTester(TestRestTemplate restTemplate, HttpMethod httpMethod, String api, Class<O> oClass) {
		this.httpMethod = httpMethod;
		this.uri = api;
		this.outputClass = oClass;
		this.restTemplate = restTemplate;
	}
	
	CheckRestResult whenInvokingisHttpStatus(I input, HttpStatus httpStatus) {
//		RestTemplate restTemplate = new RestTemplate();
//
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
//		requestFactory.setConnectTimeout(TIMEOUT);
//		requestFactory.setReadTimeout(TIMEOUT);

//		restTemplate.setRequestFactory(requestFactory);
		ResponseErrorHandlerImpl errorHandler = new ResponseErrorHandlerImpl();
		TestRestTemplate testRestTemplate = new TestRestTemplate(this.restTemplate.getRestTemplate());
		testRestTemplate.getRestTemplate().setErrorHandler(errorHandler);
		testRestTemplate.getRestTemplate().setRequestFactory(requestFactory);

		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", "application/json");

		HttpEntity<I> entity = new HttpEntity<I>(input, headers);

		return new CheckRestResult(testRestTemplate.exchange(this.uri, this.httpMethod, entity, this.outputClass), errorHandler).isResultCode(httpStatus);
	}
}