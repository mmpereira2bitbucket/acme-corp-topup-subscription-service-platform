package com.acme.app.topup.telco.offering.service.find.restapi;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.offering.service.find.FindOfferingCostService;
import com.acme.app.topup.telco.offering.service.find.FindOfferingService;
import com.acme.app.topup.telco.service.find.GetTelcoService;
import com.acme.app.topup.util.Converter;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.NotFoundValidationException;

@Named
public class FindOfferingRestApiImpl implements FindOfferingRestApi {

	private final GetTelcoService getTelcoService;
	private final FindOfferingService findOfferingService;
	private final FindOfferingCostService findOfferingCostService;

	@Inject
	public FindOfferingRestApiImpl(GetTelcoService getTelcoService, FindOfferingService findOfferingService, FindOfferingCostService findOfferingCostService) {
		super();
		Assert.notNull(getTelcoService, "GetTelcoService cannot be null");
		Assert.notNull(findOfferingService, "FindOfferingService cannot be null");
		Assert.notNull(findOfferingCostService, "FindOfferingCostService cannot be null");
		this.getTelcoService = getTelcoService;
		this.findOfferingService = findOfferingService;
		this.findOfferingCostService = findOfferingCostService;
	}

	@Override
	public Output findOneOffering(String telcoId, String offeringExternalId) throws ExtendedValidationException {
		return findOneOffering(telcoId, offeringExternalId, null);
	}
	
	@Override
	public Output findOneOffering(String telcoId, String offeringExternalId, String atDate) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telcoId);
		Assert.mandatory(Offering.Field.EXTERNAL_ID, offeringExternalId);
		LocalDate atLocalDate;
		if (null == atDate) {
			atLocalDate = LocalDate.now();
		}
		else {
			atLocalDate = Converter.fromYYYYMMDD2LocalDate(OfferingCost.Field.VALID_UNTIL, atDate);
		}

		OfferingCost offeringCost = null;
		Telco telco = this.getTelcoService.execute(telcoId);
		Offering offering = this.findOfferingService.execute(telco, offeringExternalId);
		
		Output output = null;
		if (null != offering) {
			output = new Output();
			output.offeringId = offering.getExternalId();
			output.status = offering.getStatus().name().toLowerCase();

			offeringCost = this.findOfferingCostService.execute(offering, atLocalDate);
			if (null != offeringCost) {
				output.cost = offeringCost.getCost();
				output.validFrom = Converter.fromDate2YYYYMMDD(OfferingCost.Field.VALID_FROM, offeringCost.getValidFrom());
				output.validUntil = Converter.fromDate2YYYYMMDD(OfferingCost.Field.VALID_UNTIL, offeringCost.getValidUntil());
			}
		}
		
		if (null == output) {
			throw new NotFoundValidationException(Offering.class, offeringExternalId);
		}
		
		return output;
	}

	@Override
	public List<Output> findOfferings(String telcoId) throws ExtendedValidationException {
		return findOfferings(telcoId, null);
	}
	
	@Override
	public List<Output> findOfferings(String telcoId, String atDate) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telcoId);
		LocalDate atLocalDate;
		if (null == atDate) {
			atLocalDate = LocalDate.now();
		}
		else {
			atLocalDate = Converter.fromYYYYMMDD2LocalDate(OfferingCost.Field.VALID_UNTIL, atDate);
		}

		Telco telco = this.getTelcoService.execute(telcoId);
		List<OfferingCost> offeringCosts = this.findOfferingCostService.execute(telco, atLocalDate);
		if (null == offeringCosts || offeringCosts.size() == 0) {
			throw new NotFoundValidationException(Offering.class, "telcoId=" + telco.getId());
		}
		
		List<Output> outputList = new ArrayList<>(offeringCosts.size());
		for (OfferingCost cost: offeringCosts) {
			Output output = new Output();
			output.offeringId = cost.getOffering().getExternalId();
			output.status = cost.getOffering().getStatus().name().toLowerCase();
			output.cost = cost.getCost();
			output.validFrom = Converter.fromDate2YYYYMMDD(OfferingCost.Field.VALID_FROM, cost.getValidFrom());
			output.validUntil = Converter.fromDate2YYYYMMDD(OfferingCost.Field.VALID_UNTIL, cost.getValidUntil());
			outputList.add(output);
		}
		
		return outputList;
	}
}
