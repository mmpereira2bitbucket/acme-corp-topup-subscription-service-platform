package com.acme.app.topup.util;

public class MessageKeyBuilder {

	private MessageKeyBuilder() {
	}

	public static String build(Class<?> targetObject, String fieldName, String key) {
		StringBuilder sb = new StringBuilder();
		if (null != targetObject) {
			sb.append(targetObject.getName()).append('.');
		}
		if (null != fieldName) {
			sb.append(fieldName).append('.');
		}

		return sb.append(key).toString();
	}
}
