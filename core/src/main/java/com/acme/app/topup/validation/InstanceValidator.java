package com.acme.app.topup.validation;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

public class InstanceValidator<T> {
	
	private static final Validator javaxValidator = Validation.buildDefaultValidatorFactory().getValidator();
	private static final SpringValidatorAdapter validator = new SpringValidatorAdapter(javaxValidator);

	public interface FieldErrorList {
		void rejectValue(Object rejectedValue, String messageKey, Object[] messageKeyArgs, String defaultMessage);
	}
	
	public interface FieldValidator<T> {
		void validate(T instance, FieldErrorList errors);
	}
	
	public static <T> InstanceValidator<T> createFor(Class<T> targetClass) {
		return new InstanceValidator<>(targetClass);
	}

	private final Class<T> targetClass;
	private Map<String, FieldValidator<T>> fieldValidators = null;

	private InstanceValidator(Class<T> targetClass) {
		super();
		Assert.notNull(targetClass, () -> "Target class cannot be null");
		this.targetClass = targetClass;
	}

	public T validate(T instance) throws ExtendedValidationException {
		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(instance, targetClass.getName());
		if (null == instance) {
			errors.reject(this.targetClass.getName() + ".isNull", this.targetClass.getSimpleName() + " cannot be null");
			throw new ExtendedValidationException(errors);
		}
		

		validator.validate(instance, errors);
		if (null != this.fieldValidators) {
			for (Map.Entry<String, FieldValidator<T>> v: this.fieldValidators.entrySet()) {
				if (null == errors.getFieldError(v.getKey())) {
					v.getValue().validate(instance, (rejectedValue, k, a, d) -> errors.addError(
							new FieldError(errors.getObjectName(), v.getKey(), rejectedValue, false, new String[]{k}, a, d)));
				}
			}
		}
		
        if (errors.getAllErrors().isEmpty())
            return instance;
        else {
            throw new ExtendedValidationException(errors);
        }		
	}
	
	public InstanceValidator<T> addFieldValidator(String fieldName, FieldValidator<T> v) {
		Assert.notNull(fieldName, () -> "fieldName cannot be null");
		Assert.notNull(v, () -> "FieldValidator cannot be null");
		
		try {
			this.targetClass.getField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			throw new IllegalArgumentException("Field named [" + fieldName + "] does not belongs to " + targetClass);
		}
		
		if (null == this.fieldValidators) {
			this.fieldValidators = new HashMap<>();
		}
		
		if (null != this.fieldValidators.putIfAbsent(fieldName, v)) {
			throw new IllegalArgumentException("There is already a FieldValidator associated with the field " + fieldName);
		}
		
		return this;
	}
}
