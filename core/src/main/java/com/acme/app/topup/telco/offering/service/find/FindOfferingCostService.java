package com.acme.app.topup.telco.offering.service.find;

import java.time.LocalDate;
import java.util.List;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.service.Service;
import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.validation.ExtendedValidationException;

/**
 * FindOfferingCostService performs two types of search:
 * <p>
 * <ul>
 * <li>Without using atDate, it returns the offeringCost whose atDate is
 * null or it is the Max(validUntil) AND offering is not cancelled</li>
 * <li>Using atDate, it returns the offeringCost whose validUntil is lesser
 * or equals to the given validUntil and offering is not cancelled</li>
 * </ul>
 * 
 * @author Marcelo
 *
 */
public interface FindOfferingCostService extends Service {

	@EventSource
	OfferingCost execute(Offering offering, LocalDate atDate) throws ExtendedValidationException;

	@EventSource
	List<OfferingCost> execute(Telco telco, LocalDate atDate) throws ExtendedValidationException;
}
