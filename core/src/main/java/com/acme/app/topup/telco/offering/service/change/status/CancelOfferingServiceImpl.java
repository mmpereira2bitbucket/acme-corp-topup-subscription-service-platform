package com.acme.app.topup.telco.offering.service.change.status;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.dao.OfferingDAO;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.NotFoundValidationException;

@Named
public class CancelOfferingServiceImpl implements CancelOfferingService {

	private final OfferingDAO offeringDAO;
	
	@Inject
	public CancelOfferingServiceImpl(OfferingDAO offeringDAO) {
		super();
		Assert.notNull(offeringDAO, "OfferingDAO is mandatory");
		this.offeringDAO = offeringDAO;
	}

	@Override
	public Offering execute(Offering offering) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.ID, offering);
		long id = Assert.mandatory(Offering.Field.ID, offering.getId());
		
		offering = this.offeringDAO.lockForUpdate(id);
		if (null == offering) {
			throw new NotFoundValidationException(Offering.class, Long.toString(id));
		}
		if (Offering.Status.CANCELLED != offering.getStatus()) {
			offering.setStatus(Offering.Status.CANCELLED);
			offering = this.offeringDAO.save(offering);
		}
		
		return offering;
	}

}
