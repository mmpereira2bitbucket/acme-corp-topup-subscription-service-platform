package com.acme.app.topup.telco.offering.service.change.restapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.acme.app.topup.service.restapi.RestApi;
import com.acme.app.topup.validation.ExtendedValidationException;

public interface ChangeOfferingRestApi extends RestApi {

	class Input {
		public String validFrom = null; // YYYYMMDD
		public String validUntil = null; // YYYYMMDD
		public Double cost = null;
		public String status;
	}
	
	class Output {
		public String offeringId = null;
		public String validFrom = null; // YYYYMMDD
		public String validUntil = null; // YYYYMMDD
		public Double cost = null;
		public String status = null;
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "offering/{id}", method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
	public Output changeOffering(@PathVariable("telcoId") String telcoId, @PathVariable("id") String offeringId,
			@RequestBody ChangeOfferingRestApi.Input input) throws ExtendedValidationException;
}
