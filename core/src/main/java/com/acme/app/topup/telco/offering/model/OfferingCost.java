package com.acme.app.topup.telco.offering.model;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.acme.app.topup.util.FieldFinder;

/**
 * OfferingCost represents the cost to renew the associated offering during the
 * recurrence procedure
 * 
 */
@Entity
public class OfferingCost {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	/**
	 * Offering associated with this cost
	 */
	@ManyToOne
	@NotNull
	private Offering offering = null;

	/**
	 * Since when the cost is valid
	 */
	@NotNull
	private Date validFrom = null;
	
	/**
	 * Until when the cost is valid
	 */
	private Date validUntil = null;

	/**
	 * Cost of the offering when acquired
	 */
	private double cost;
	
	public long getId() {
		return id;
	}

	public Offering getOffering() {
		return offering;
	}

	public void setOffering(Offering offering) {
		this.offering = offering;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	
	public void setValidFrom(LocalDate validFrom) {
		this.validFrom = null == validFrom? null : Date.valueOf(validFrom);
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public void setValidUntil(LocalDate validUntil) {
		this.validUntil = null == validUntil? null : Date.valueOf(validUntil);
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public interface Field {
		java.lang.reflect.Field ID = FieldFinder.get(OfferingCost.class, "id");
		java.lang.reflect.Field OFFERING = FieldFinder.get(OfferingCost.class, "offering");
		java.lang.reflect.Field VALID_FROM = FieldFinder.get(OfferingCost.class, "validFrom");
		java.lang.reflect.Field VALID_UNTIL = FieldFinder.get(OfferingCost.class, "validUntil");
		java.lang.reflect.Field COST = FieldFinder.get(OfferingCost.class, "cost");
	}
}
