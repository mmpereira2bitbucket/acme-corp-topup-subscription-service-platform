package com.acme.app.topup.telco.offering.service.cancel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.service.change.status.CancelOfferingServiceImpl;
import com.acme.app.topup.telco.offering.service.find.TestFindOfferingCostServiceImpl;
import com.acme.app.topup.telco.service.AbstractServiceTester;
import com.acme.app.topup.validation.ExtendedValidationException;

@SpringBootTest(classes = { TestFindOfferingCostServiceImpl.class })
public class TestCancelOfferingServiceImpl extends AbstractServiceTester {
	private static final Telco TELCO = new Telco("TELCO");
	private static final String TURBO_OFFER = "TURBO";

	private CancelOfferingServiceImpl cancelOfferingService = null;
	
	@Before
	public void setup() {
		this.cancelOfferingService = new CancelOfferingServiceImpl(offeringDAO);
	}

	@Test
	public void testSuccessCancelExistentOffering() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		
		try {
			assertEquals(offering, this.cancelOfferingService.execute(offering));
		}
		catch(ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expect");
		}
	}

	@Test
	public void testSuccessCancelCancelledOffering() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		offering = this.cancelOffering(offering);
		
		try {
			this.cancelOfferingService.execute(offering);
		}
		catch(ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expect");
		}
	}
}
