package com.acme.app.topup.telco.offering.service.find;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.service.AbstractServiceTester;
import com.acme.app.topup.validation.ExtendedValidationException;

@SpringBootTest(classes={TestFindOfferingCostServiceImpl.class})
public class TestFindOfferingCostServiceImpl extends AbstractServiceTester {
	private static final Telco TELCO = new Telco("TELCO");
	private static final String TURBO_OFFER = "TURBO";
	private static final String TURBO_OFFER_2 = "TURBO2";
	private static final LocalDate TODAY = LocalDate.now();
	private static final LocalDate TOMORROW = TODAY.plus(1, ChronoUnit.DAYS);
	private static final LocalDate YESTERDAY = TODAY.minus(1, ChronoUnit.DAYS);
	private static final LocalDate DAY_BEFORE_YESTERDAY = YESTERDAY.minus(1, ChronoUnit.DAYS);
	private static final Double COST_1 = 10.43;
	
	private FindOfferingCostServiceImpl findOfferingCostService;
	
	@Before
	public void setup () {
		this.findOfferingCostService = new FindOfferingCostServiceImpl(offeringCostDAO);
	}
	
	@Test
	public void testWithoutValidUntilSuccessFindOfOfferingWithNoValidity() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		createOfferingCost(offering, YESTERDAY, null, COST_1);
		
		try {
			OfferingCost offeringCost = findOfferingCostService.execute(offering, null);
			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, YESTERDAY, null, COST_1);
			checkOffering(offeringCost.getOffering(), TELCO, TURBO_OFFER, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testWithoutValidUntilSuccessFindOfOfferingWithValidity() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		createOfferingCost(offering, YESTERDAY, TOMORROW, COST_1);
		
		try {
			OfferingCost offeringCost = findOfferingCostService.execute(offering, null);
			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, YESTERDAY, TOMORROW, COST_1);
			checkOffering(offeringCost.getOffering(), TELCO, TURBO_OFFER, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testWithoutValidUntilSuccessFindOfOfferingWithValidityInPast() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		OfferingCost offeringCost = createOfferingCost(offering, DAY_BEFORE_YESTERDAY, TODAY, COST_1);

		offeringCost.setValidUntil(YESTERDAY);
		offeringCostDAO.save(offeringCost);
		
		try {
			offeringCost = findOfferingCostService.execute(offeringCost.getOffering(), null);
			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, DAY_BEFORE_YESTERDAY, YESTERDAY, COST_1);
			checkOffering(offeringCost.getOffering(), TELCO, TURBO_OFFER, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testWithValidUntilTodayWithOfferingWithValidityInPastNoOutputExpected() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		OfferingCost offeringCost = createOfferingCost(offering, DAY_BEFORE_YESTERDAY, TODAY, COST_1);

		offeringCost.setValidUntil(YESTERDAY);
		offeringCostDAO.save(offeringCost);
		
		try {
			offeringCost = findOfferingCostService.execute(offeringCost.getOffering(), TODAY);
			assertEquals(null, offeringCost);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testWithValidUntilTodayWithOfferingWithValidityInFuture_OutputExpected() {
		Offering offering = createOffering(TELCO, TURBO_OFFER);
		createOfferingCost(offering, DAY_BEFORE_YESTERDAY, TOMORROW, COST_1);

		try {
			OfferingCost offeringCost = findOfferingCostService.execute(offering, TODAY);
			assertNotNull(offeringCost);
			checkOfferingCost(offeringCost, DAY_BEFORE_YESTERDAY, TOMORROW, COST_1);
			checkOffering(offeringCost.getOffering(), TELCO, TURBO_OFFER, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}

	@Test
	public void testWithoutValidUntilSuccessFindOfOfferingWithNoValidityTwoTelcos() {
		Offering offering_1 = createOffering(TELCO, TURBO_OFFER);
		Offering offering_2 = createOffering(TELCO, TURBO_OFFER_2);
		createOfferingCost(offering_1, YESTERDAY, null, COST_1);
		createOfferingCost(offering_2, YESTERDAY, null, COST_1);

		try {
			List<OfferingCost> costs = findOfferingCostService.execute(TELCO, null);
			assertNotNull(costs);
			assertEquals(2, costs.size());
			checkOfferingCost(costs.get(0), YESTERDAY, null, COST_1);
			checkOffering(costs.get(0).getOffering(), TELCO, TURBO_OFFER, Offering.Status.ACTIVE);
			checkOfferingCost(costs.get(1), YESTERDAY, null, COST_1);
			checkOffering(costs.get(1).getOffering(), TELCO, TURBO_OFFER_2, Offering.Status.ACTIVE);
		} catch (ExtendedValidationException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
}
