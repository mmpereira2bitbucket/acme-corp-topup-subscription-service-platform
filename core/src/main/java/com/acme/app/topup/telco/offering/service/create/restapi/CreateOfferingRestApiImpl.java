package com.acme.app.topup.telco.offering.service.create.restapi;

import java.time.LocalDate;

import javax.inject.Inject;
import javax.inject.Named;

import com.acme.app.topup.telco.model.Telco;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingCostService;
import com.acme.app.topup.telco.offering.service.create.CreateOfferingService;
import com.acme.app.topup.telco.service.find.GetTelcoService;
import com.acme.app.topup.util.Converter;
import com.acme.app.topup.validation.Assert;
import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.ExtendedValidationException.ValidationType;

@Named
public class CreateOfferingRestApiImpl implements CreateOfferingRestApi {

	private final GetTelcoService getTelcoService;
	private final CreateOfferingService createOfferingService;
	private final CreateOfferingCostService createOfferingCostService;

	@Inject
	public CreateOfferingRestApiImpl(CreateOfferingService createOfferingService, GetTelcoService getTelcoService,
			CreateOfferingCostService createOfferingCostService) {
		super();
		Assert.notNull(createOfferingCostService, "CreateOfferingCostService cannot be null");
		Assert.notNull(createOfferingService, "CreateOfferingService cannot be null");
		Assert.notNull(getTelcoService, "GetTelcoService cannot be null");
		this.createOfferingService = createOfferingService;
		this.getTelcoService = getTelcoService;
		this.createOfferingCostService = createOfferingCostService;
	}

	@Override
	public Void createOffering(String telcoId, Input input) throws ExtendedValidationException {
		Assert.mandatory(Offering.Field.OWNER, telcoId);
		Assert.mandatory(Offering.Field.EXTERNAL_ID, input.offeringId);
		Assert.mandatory(OfferingCost.Field.COST, input.cost);
		Assert.mandatory(OfferingCost.Field.VALID_FROM, input.validFrom);
		LocalDate validFrom = Converter.fromYYYYMMDD2LocalDate(OfferingCost.Field.VALID_FROM, input.validFrom);
		LocalDate validUntil = Converter.fromYYYYMMDD2LocalDate(OfferingCost.Field.VALID_UNTIL, input.validUntil);

		Telco telco = this.getTelcoService.execute(telcoId);
		Offering offering = this.createOfferingService.execute(telco, input.offeringId);
		
		Assert.notNull(this.createOfferingCostService.execute(offering, validFrom, validUntil, input.cost),
				() -> new ExtendedValidationException(OfferingCost.Field.COST,
						String.format("[cost=%s;ValidFrom=%s;ValidUntil=%s]", input.cost, validFrom, validUntil),
						ValidationType.MANDATORY));
		return null;
	}

}
