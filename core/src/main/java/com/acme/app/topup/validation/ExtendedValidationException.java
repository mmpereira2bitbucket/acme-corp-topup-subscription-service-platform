package com.acme.app.topup.validation;

import java.lang.reflect.Field;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.acme.app.topup.util.MessageKeyBuilder;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ExtendedValidationException extends javax.validation.ValidationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7756604375809201425L;

	public enum ValidationType {
		MANDATORY, DUPLICATED, NOT_FOUND, NONPOSITIVE_INTEGER, PERMITTED_VALUE
	}

	public Errors errors = null;

	private static String buildDefaultMessage(Class<?> objectClass, String fieldName, Object rejectedValue,
			String validationType) {
		return new StringBuilder(null == validationType ? "" : validationType)
				.append(null == validationType ? "" : ": ").append("Invalid value [").append(rejectedValue)
				.append("] for field [").append(fieldName).append("] of ")
				.append(null == objectClass ? "unknown" : objectClass.getName()).toString();
	}

	public ExtendedValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExtendedValidationException(Field field, Object rejectedValue, ValidationType validationType) {
		this(field.getDeclaringClass(), field.getName(), rejectedValue, validationType, null, null);
	}

	public ExtendedValidationException(Field field, Object rejectedValue, ValidationType validationType, Object[] validationTypeArgs) {
		this(field.getDeclaringClass(), field.getName(), rejectedValue, validationType, validationTypeArgs, null);
	}

	public ExtendedValidationException(Field field, Object rejectedValue, ValidationType validationType, Object[] validationTypeArgs, Throwable cause) {
		this(field.getDeclaringClass(), field.getName(), rejectedValue, validationType, validationTypeArgs, null, cause);
	}

	public ExtendedValidationException(Field field, Object rejectedValue, String constraint, Object[] validationTypeArgs) {
		this(field.getDeclaringClass(), field.getName(), rejectedValue, constraint, validationTypeArgs, null, null);
	}

	public ExtendedValidationException(Field field, Object rejectedValue, String constraint) {
		this(field.getDeclaringClass(), field.getName(), rejectedValue, constraint, null, null, null);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue,
			ValidationType validationType) {
		this(objectClass, fieldName, rejectedValue, validationType, null, null);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue, String constraintName) {
		this(objectClass, fieldName, rejectedValue, constraintName, null, null, null);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue,
			ValidationType validationType, Object[] validationTypeArgs) {
		this(objectClass, fieldName, rejectedValue, validationType, validationTypeArgs, null, null);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue, String constraintName,
			Object[] validationTypeArgs) {
		this(objectClass, fieldName, rejectedValue, constraintName, validationTypeArgs, null, null);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue,
			ValidationType validationType, Object[] validationTypeArgs, String defaultMessage, Throwable ex) {
		this(objectClass, fieldName, rejectedValue,
				null == validationType ? null : MessageKeyBuilder.build(objectClass, fieldName, validationType.name()),
				validationTypeArgs, null, ex);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue,
			ValidationType validationType, Object[] validationTypeArgs, Throwable ex) {
		this(objectClass, fieldName, rejectedValue,
				null == validationType ? null : MessageKeyBuilder.build(objectClass, fieldName, validationType.name()),
				validationTypeArgs, null, ex);
	}

	public ExtendedValidationException(Class<?> objectClass, String fieldName, Object rejectedValue, String constraintName,
			Object[] validationTypeArgs, String defaultMessage, Throwable ex) {
		super(null == defaultMessage ? buildDefaultMessage(objectClass, fieldName, rejectedValue, constraintName)
				: defaultMessage, ex);
		BeanPropertyBindingResult errs = new BeanPropertyBindingResult(null, objectClass.getName());
		errs.addError(new FieldError(objectClass.getName(), fieldName, rejectedValue, false,
				new String[] { constraintName },
				null == validationTypeArgs ? new Object[] { rejectedValue } : validationTypeArgs, getMessage()));
		this.errors = errs;
	}

	public ExtendedValidationException(Errors errors) {
		super(null == errors ? null : errors.toString());
		this.errors = errors;
	}

	public ExtendedValidationException(String message) {
		super(message);
	}

	public Errors getErrors() {
		return errors;
	}
}
