package com.acme.app.topup.util;

import java.lang.reflect.Field;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import com.acme.app.topup.validation.ExtendedValidationException;
import com.acme.app.topup.validation.ExtendedValidationException.ValidationType;

public class Converter {

	private Converter() {
	}

	public static LocalDate fromYYYYMMDD2LocalDate(String value) {
		LocalDate result = null;
		if (null != value) {
			try {
			result = LocalDate.parse(value, java.time.format.DateTimeFormatter.BASIC_ISO_DATE);
			}
			catch(DateTimeParseException  ex) {
				throw new ExtendedValidationException(null, value, ValidationType.PERMITTED_VALUE,
						new Object[] {value, "YYYYMMDD"}, ex);
			}
		}
		
		return result;
	}

	public static LocalDate fromYYYYMMDD2LocalDate(Field field, String value) {
		LocalDate result = null;
		if (null != value) {
			try {
				result = LocalDate.parse(value, java.time.format.DateTimeFormatter.BASIC_ISO_DATE);
			}
			catch(DateTimeParseException  ex) {
				throw new ExtendedValidationException(field, value, ValidationType.PERMITTED_VALUE,
						new Object[] {value, "YYYYMMDD"}, ex);
			}
		}
		
		return result;
	}

	public static String fromDate2YYYYMMDD(Field field, Date value) {
		return fromLocalDate2YYYYMMDD(field, null == value? null: value.toLocalDate());
	}
	
	public static String fromLocalDate2YYYYMMDD(Field field, LocalDate value) {
		String result = null;
		if (null != value) {
			try {
				result = value.format(java.time.format.DateTimeFormatter.BASIC_ISO_DATE);
			}
			catch(DateTimeParseException  ex) {
				throw new ExtendedValidationException(field, value, ValidationType.PERMITTED_VALUE,
						new Object[] {value, "YYYYMMDD"}, ex);
			}
		}
		
		return result;
	}
}
