package com.acme.app.topup;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.acme.app.topup.telco.offering.service.cancel.TestCancelOfferingServiceImpl;
import com.acme.app.topup.telco.offering.service.create.TestCreateOfferingCostServiceImpl;
import com.acme.app.topup.telco.offering.service.find.TestFindOfferingCostServiceImpl;
import com.acme.app.topup.telco.offering.service.restapi.TestOfferingRestApiController;

@RunWith(Suite.class)
@SuiteClasses({ TestCancelOfferingServiceImpl.class, TestCreateOfferingCostServiceImpl.class,
		TestCancelOfferingServiceImpl.class, TestFindOfferingCostServiceImpl.class, TestOfferingRestApiController.class })
public class AllTests {

}
