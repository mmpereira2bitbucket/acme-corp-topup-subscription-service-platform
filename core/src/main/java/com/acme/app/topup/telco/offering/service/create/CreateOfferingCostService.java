package com.acme.app.topup.telco.offering.service.create;

import java.time.LocalDate;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.acme.app.topup.event.EventSource;
import com.acme.app.topup.service.Service;
import com.acme.app.topup.telco.offering.model.Offering;
import com.acme.app.topup.telco.offering.model.OfferingCost;
import com.acme.app.topup.validation.ExtendedValidationException;

public interface CreateOfferingCostService extends Service {

	@EventSource
	@Transactional(propagation = Propagation.REQUIRED)
	OfferingCost execute(Offering offering, LocalDate validFrom, LocalDate validUntil, double cost)
			throws ExtendedValidationException;
}
