package com.acme.app.topup.telco.reload.service.probe;

import java.io.IOException;
import java.util.UUID;

import com.acme.app.topup.telco.reload.service.TelcoResponse;
import com.acme.app.topup.validation.ExtendedValidationException;

public interface ProbeReloadService {

	class Output extends TelcoResponse {
		public Output(String id) {
			super(id);
		}

		boolean reloadDone = false;
	}
	
	Output execute(UUID requestId,String reloadRefForProbing, String msisdn, String offeringExternalId, double cost) throws ExtendedValidationException, IOException;
}
