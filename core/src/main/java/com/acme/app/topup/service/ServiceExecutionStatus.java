package com.acme.app.topup.service;

public class ServiceExecutionStatus {
	public enum ErrorType {TEMPORARY, UNDEFINED, PERMANENT};

	public static final ServiceExecutionStatus SUCCESS = new ServiceExecutionStatus(); 
	public final int code;
	public final ErrorType errorType;
	public final String description;
	
	private ServiceExecutionStatus() {
		this.code = 0;
		this.errorType = null;
		this.description = null;
	}
	
	public ServiceExecutionStatus(int code, ErrorType errorType, String description) {
		this.code = code;
		this.errorType = SUCCESS.code == code? null: null == errorType? ErrorType.UNDEFINED: errorType;
		this.description = SUCCESS.code == code? null: description;
	}
}
